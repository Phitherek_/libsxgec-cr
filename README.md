# LibSXGEC - Simple XRandR Global Event Catcher - Crystal bindings

## Prerequisites

* libsxgec (https://gitlab.com/Phitherek\_/libsxgec)

## Usage

* Add this shard to dependencies
* libsxgec bindings are implemented in sxgec.cr file
* See libsxgec repository for documentation and usage instructions

## Bindings

* `sxgec_init` - `LibSXGEC.init`
* `sxgec_catch_next_event` - `LibSXGEC.catch_next_event`
* `sxgec_catch_events` - `LibSXGEC.catch_events`
* `sxgec_free` - `LibSXGEC.free`

# Contributing

By all means!
