@[Link("sxgec")]
lib LibSXGEC
    fun init = sxgec_init(randr_screen_change: Int32, randr_crtc_change: Int32, randr_output_change: Int32, randr_output_property: Int32, randr_provider_change: Int32, randr_provider_property: Int32, randr_resource_change: Int32, randr_lease: Int32) : Int32
    fun catch_next_event = sxgec_catch_next_event(callback : (LibC::Char*, Void*) -> Void, data: Void*) : Int32
    fun catch_events = sxgec_catch_events(randr_screen_change: Int32, randr_crtc_change: Int32, randr_output_change: Int32, randr_output_property: Int32, randr_provider_change: Int32, randr_provider_property: Int32, randr_resource_change: Int32, randr_lease: Int32, callback : (LibC::Char*, Void*) -> Void, data: Void*) : Void
    fun free = sxgec_free : Void
end
