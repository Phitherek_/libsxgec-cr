require "./libsxgec/sxgec"

LibSXGEC.init(1,1,1,1,1,1,1,1)
sxgec_callback = ->(event_name : UInt8*, data: Void*) { puts String.new(event_name) }
loop do
    break if LibSXGEC.catch_next_event(sxgec_callback, nil) == 1
end
LibSXGEC.free()
